import express from 'express'
import gameScores from '../controllers/scoring'
import game from '../controllers/games'

const router = express.Router()

router.post('/new', game.createNew)
router.post('/game/:gameId/:frameNumber', gameScores.setGameScores.bind(gameScores))
router.get('/game/:gameId/:frameNumber?', gameScores.getGameScores)


export default router
