import { Pool } from 'pg'

if (!process.env.PRODUCTION) {
  require('dotenv').config()
}

const db = new Pool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  port: process.env.DB_PORT
})

export default db
