import frameHelper from '../helpers/frames'
import scoreHelper from '../helpers/scores'
import gamesModel from '../models/games'
import framesModel from '../models/frames'
import frameTurnsModel from '../models/frameTurns'

class scoringController {
  getGameScores (request, response) {
    const gameId = request.params.gameId
    let frame = null

    if (request.params.hasOwnProperty('frameNumber')) {
      if (typeof request.params.frameNumber !== 'undefined') {
        frame = request.params.frameNumber
      }
    }

    frameHelper.frameNumber(frame).then((frame) => { // check to make sure frame number is valid
      gamesModel.fetchGameInfo(gameId, frame)
      .then((gameInfo) => {
        // console.log(gameInfo)
        if (gameInfo.length > 0) {
          let gameResponse = {
            gameScore: (() => {
              // Get player IDs
              const player1Id = gameInfo[0].player1id
              const player2Id = gameInfo[0].player2id

              // Split results into per-player turns
              let playerTurns = {}

              // Player turns
              for (let i = 0; i < gameInfo.length; i++) {
                switch (gameInfo[i].players_id) {
                  case player1Id:
                  if (!playerTurns.hasOwnProperty('player1')) {
                    playerTurns.player1 = []
                  }
                  playerTurns.player1.push(gameInfo[i])
                  break

                  case player2Id:
                  if (!playerTurns.hasOwnProperty('player2')) {
                    playerTurns.player2 = []
                  }
                  playerTurns.player2.push(gameInfo[i])
                  break
                }
              }

              // console.log(playerTurns)

              // Get player names
              const playerNames = (() => {
                let returnObj = {}

                for (let player in playerTurns) {
                  if (playerTurns.hasOwnProperty(player)) {
                    returnObj[player] = playerTurns[player][0].playername
                  }
                }

                return returnObj
              })()

              // console.log(playerNames)


              // Calculate scores per player
              let returnVal = {}
              returnVal[playerNames.player1 + '-' + player1Id] = scoreHelper.playerScore(playerTurns.player1, 1)

              if (playerNames.hasOwnProperty('player2')) {
                returnVal[playerNames.player2 + '-' + player2Id] = scoreHelper.playerScore(playerTurns.player2, 2)
              }

              return returnVal
            })(),
            framesHistory: scoreHelper.gameHistory(gameInfo)
          }

          return response.status(200).json(gameResponse)
        } else {
          let errorMessage = 'The combination of game ID and frame ID you have specified could not be found'

          if (frame === null) {
            errorMessage = 'A game with at least 1 played frame, with this ID, could not be found.'
          }

          return response.status(404).json({
            success: false,
            message: errorMessage
          })
        }
      })
      .catch(() => {
        return response.status(500).json({
          success: false,
          message: 'There was a problem communicating with the database'
        })
      })
    })
    .catch((error) => {
      return response.status(500).json({
        success: false,
        message: error
      })
    })
  }

  createOrUpdateFrameTurns (frameId, playerScores) {
    return new Promise ((resolve, reject) => {
      let player1Scores = null
      let player2Scores = null

      if (playerScores.hasOwnProperty('player1')) {
        if (playerScores.player1.hasOwnProperty('scores')) {
          player1Scores = playerScores.player1.scores
        }
      }

      if (playerScores.hasOwnProperty('player2')) {
        if (playerScores.player2.hasOwnProperty('scores')) {
          player1Scores = playerScores.player2.scores
        }
      }


      if ((player2Scores !== null && player1Scores.length === player2Scores.length) || player2Scores === null) {
        // Check if frameTurns exist
        frameTurnsModel.fetchFrameTurns(frameId)
        .then((frameTurns) => {
          if (frameTurns.length) {
            // Frame turns already exist
            let frameTurn1Scores = [
              player1Scores[0]
            ]

            if (player2Scores !== null) {
              frameTurn1Scores.push(player2Scores[0])
            }

            frameTurnsModel.updateFrameTurn(frameTurns[0].frameturns_id, frameTurn1Scores)
            .then((updatedFrameTurn1) => {
              if (frameTurns.length > 1) {
                let frameTurn2Scores = [
                  player1Scores[1]
                ]

                if (player2Scores !== null) {
                  frameTurn2Scores.push(player2Scores[1])
                }

                frameTurnsModel.updateFrameTurn(frameTurns[1].frameturns_id, frameTurn2Scores)
                .then(() => {
                  resolve(true) // Turns 1 and 2 updated
                })
                .catch(() => {
                  reject('Could not update turn 2, but have updated turn 1')
                })
              } else {
                resolve(true) // Turn 1 updated
              }
            })
            .catch(() => {
              reject('Could not update turn 1')
            })
          } else {
            let firstNewFrameTurnScores = [
              player1Scores[0]
            ]

            if (player2Scores !== null) {
              firstNewFrameTurnScores.push(player2Scores[0])
            }

            frameTurnsModel.createFrameTurn(firstNewFrameTurnScores, frameId, 1)
            .then(() => {
              if (player1Scores.length > 1) { // we already know both player1 and player2 scores should be the same length at this point
                let secondNewFrameTurnScores = [
                  player1Scores[1]
                ]

                if (player2Scores !== null) {
                  secondNewFrameTurnScores.push(player2Scores[1])
                }

                frameTurnsModel.createFrameTurn(secondNewFrameTurnScores, frameId, 2)
                .then(() => {
                  resolve(true) // Created turn 1 and 2
                })
                .catch(() => {
                  reject('Could not create turn 2, but created turn 1')
                })
              } else {
                resolve(true) // created turn 1
              }
            })
            .catch(() => {
              reject('Could not create turn 1')
            })
          }
        })
        .catch(() => {
          reject('Could not check if frame turns exist')
        })
      } else {
        reject('You need to supply the same number of turns for both player 1 and 2')
      }
    })
  }


  setGameScores (request, response) {
    const gameId = request.params.gameId
    const frame = request.params.frameNumber

    let scoresOK = true
    for (let player in request.body.game) {
      if (request.body.game.hasOwnProperty(player)) {
        let turn1 = request.body.game[player].turn1
        let turn2 = 0

        if (turn1 < 0 || turn1 > 10) {
          scoresOK = false
          break
        }

        if (request.body.game[player].hasOwnProperty('turn2')) {
          turn2 = request.body.game[player].turn2

          if (turn2 < 0 || turn2 > 10) {
            scoresOK = false
            break
          }
        }

        if (turn1 + turn2 > 10 || turn1 + turn2 < 0) {
          scoresOK = false
          break
        }
      }
    }

    if (scoresOK) {
      frameHelper.frameNumber(frame)
      .then((frame) => {
        gamesModel.fetchGameConfig(gameId) // Tells us the if the game ID exists, and what players have been assigned
        .then((gameConfig) => {
          if (gameConfig.length > 0) {
            // Sort / filter player information
            let gamePlayers = {}
            const playersToGamesId = gameConfig[0].playerstogames_id

            for (let i = 0; i < gameConfig.length; i++) {
              if (gameConfig[i].players_id === gameConfig[i].player1id) {
                gamePlayers.player1 = {
                  playerName: gameConfig[i].playername,
                  playerId: gameConfig[i].players_id
                }
              }

              if (gameConfig[i].players_id === gameConfig[i].player2id) {
                gamePlayers.player2 = {
                  playerName: gameConfig[i].playername,
                  playerId: gameConfig[i].players_id
                }
              }
            }

            // Check if supplied playerIDs match what we have above, and assign scores
            for (let player in gamePlayers) {
              if (gamePlayers.hasOwnProperty(player)) {
                gamePlayers[player].scores = (() => {
                  const playerKey = gamePlayers[player].playerName + '-' + gamePlayers[player].playerId
                  if (request.body.game.hasOwnProperty(playerKey)) {
                    return [
                      request.body.game[playerKey].turn1,
                      request.body.game[playerKey].turn2
                    ]
                  } else {
                    return false
                  }
                })()
              }
            }

            // Make sure player Ids match
            if ((gamePlayers.hasOwnProperty('player1') && !gamePlayers.hasOwnProperty('player2') && gamePlayers.player1.scores !== false) || (gamePlayers.hasOwnProperty('player1') && gamePlayers.hasOwnProperty('player2') && gamePlayers.player1.scores !== false && gamePlayers.player2.scores !== false)) {
              // Check if we need to create a new frame
              framesModel.checkFrameExists(playersToGamesId, frame)
              .then((frameResponse) => {
                // console.log(frameResponse)
                // Frame doesn't exist yet - so we need to create one
                if (!frameResponse.length) {
                  framesModel.createNewFrame(playersToGamesId, frame)
                  .then((createdFrame) => {
                    const frameId = createdFrame[0].frames_id
                    this.createOrUpdateFrameTurns(frameId, gamePlayers)
                    .then(() => {
                      this.getGameScores(request, response)
                    })
                    .catch((error) => {
                      return response.status(500).json({
                        success: false,
                        message: error
                      })
                    })
                  })
                  .catch(() => {
                    return response.status(500).json({
                      success: false,
                      message: 'Could not create a new frame'
                    })
                  })
                } else {
                  const existingFrameId = frameResponse[0].frames_id
                  this.createOrUpdateFrameTurns(existingFrameId, gamePlayers)
                  .then(() => {
                    this.getGameScores(request, response)
                  })
                  .catch((error) => {
                    return response.status(500).json({
                      success: false,
                      message: error
                    })
                  })
                }
              })
              .catch(() => {
                return response.status(500).json({
                  success: false,
                  message: 'There was a problem checking if this frame exists.'
                })
              })
            } else {
              return response.status(500).json({
                success: false,
                message: 'The player IDs you specified do not match up with this game'
              })
            }
          } else {
            return response.status(404).json({
              success: false,
              message: 'Either this game ID doesn\'t exist, or there are no players assigned to it.'
            })
          }
        })
      })
      .catch((error) => {
        return response.status(500).json({
          success: false,
          message: error
        })
      })
    } else {
      response.status(500).json({
        success: false,
        message: 'The scores for 1 or more players specified are not valid.'
      })
    }
  }
}

const gameScores = new scoringController()
export default gameScores
