import playerHelper from '../helpers/players'
import gamesModel from '../models/games'
import playersModel from '../models/players'

// Note to whoever has to look at this controller:
// Apologies for how messy it is. Running out of time at this point...

class gamesController {
  createNew (request, response) {
    playerHelper.validatePlayers(request.body)
    .then((players) => {
      playersModel.createNewPlayers(players)
      .then((newPlayerRecords) => {
        let playerIds = {
          player1Id: newPlayerRecords[0].playername + '-' + newPlayerRecords[0].players_id
        }

        if (newPlayerRecords.length > 1) {
          playerIds.player2Id = newPlayerRecords[1].playername + '-' + newPlayerRecords[1].players_id
        }

        gamesModel.createNewGame()
        .then((newGameRecord) => {
          const newGameId = newGameRecord.games_id

          let playersToAssign = []
          for (let i = 0; i < newPlayerRecords.length; i++) {
            playersToAssign.push(newPlayerRecords[i].players_id)
          }

          gamesModel.assignPlayersToGame(playersToAssign, newGameId)
          .then((playersToGame) => {
            let newGameResponse = {
              gameId: newGameId,
              player1Id: playerIds.player1Id
            }

            if (playerIds.hasOwnProperty('player2Id')) {
              newGameResponse.player2Id = playerIds.player2Id
            }

            return response.status(200).json(newGameResponse)
          })
          .catch((error) => {
            return response.status(500).json(error)
          })
        })
        .catch((error) => {
          return response.status(500).json(error)
        })
      })
      .catch((error) => {
        return response.status(500).json(error)
      })
    })
    .catch((error) => {
      return response.status(500).json(error)
    })
  }
}

const game = new gamesController()
export default game
