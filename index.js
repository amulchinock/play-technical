if (!process.env.PRODUCTION) {
  require('dotenv').config()
}

import express from 'express'
import router from './router'

const app = express()
const port = process.env.PORT || 8080

app.use(express.json())
app.use(router)

app.listen(port, () => console.log(`Listening on port ${port}`))
