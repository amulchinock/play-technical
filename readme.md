# Let's Play!

## API Location
### Remote
You can find the API here: http://playtechnical-env.zjpjxbbrtn.eu-west-2.elasticbeanstalk.com

### Local
If you wish to run it locally - do the following:

- In your terminal window, run ```git clone https://bitbucket.org/amulchinock/play-technical.git```

- In your teminal again, run ```npm install```

- Navigate to the API's root directory, and create a file named ```.env```

- Populate the ```.env``` file with values sent accross via email

- Now run ```npm run dev```

The API will now be available to you locally at ```http://localhost:8080```

## Routes
**Quick tip! A Postman collection has been included with this project, should you wish to use it. See ```Play Technical.postman_collection.json``` in the project's root directory.**

### ```POST``` /game/new
Creates a new game.

#### Request
Accepts the following, encapsulated in a ```players``` request object:

|         | Description          | Type   | Required |
|---------|----------------------|--------|----------|
| player1 | The name of player 1 | String | true     |
| player2 | The name of player 2 | String | false    |

For example:
```
// Both players specified
{
	"players": {
		"player1": "Bill",
		"player2": "Ben"
	}
}

// A single player specified
{
  "players": {
    "player1": "Bill"
  }
}
```

#### Response
API will respond, if successful in creating a new game, with the following:

|           | Description                                                                                                                    | Type   |
|-----------|--------------------------------------------------------------------------------------------------------------------------------|--------|
| gameId    | Unique ID for the new game that has been created. Can be used later to look up game information, or resume an unfinished game. | String |
| player1Id | Unique ID for Player 1. Can be used later to look up player scores.                                                            | String |
| player2Id | Unique ID for Player 2 (if Player 2 was specified). Can be used later to look up player scores.                                | String |

Example:
```
{
    "gameId": 34,
    "player1Id": "Bill-105",
    "player2Id": "Ben-106"
}
```

### ```POST``` /game/:gameId/:frameNumber
Adds score information for each frame in the game.

#### Request
URL parameters accept the following:

| Parameter | Description |
|-----------|-------------|
| ```:gameId``` | A valid game ID, as created from ```/new``` |
| ```:frameNumber``` | The current frame of the game. Accepts any integer from 1 - 10. |

The request body should contain the following, inside a ```game``` request object:

| | Description | Type | Required |
|-|-------------|------|----------|
| ```:player1Id``` | An object, keyed on the ID of Player 1, as created from ```/new```. Object contains player score information, as detailed below. | Object | True |
| ```:player2Id``` | An object, keyed on the ID of Player 2, as created from ```/new```. Object contains player score information, as detailed below. | Object | False |

Example:
```
// Assuming ID for player 1 is ABC-123-456

{
  "game": {
    "ABC-123-456": { ... }
  }
}
```

The player score object(s) are structured as follows:

| | Description | Type | Required |
|-|-------------|------|----------|
| turn1 | Score achieved on player's first turn | Integer | True |
| turn2 | Score achieved on player's second turn | Integer | False |

The API will calculate the overall game score, and keep track of the final score for Strikes and Spares in each frame. **Therefore, you only need to provide the score for each turn - the API will do the rest.**

---

***Important: making a request to the same frame, with different values (assuming the user IDs are correct), will overwrite the previous values set.***

***Useful if you made a mistake and want to go back to correct it. Dangerous if you forget to advance to the next frame before submitting your values.***

---

Example:
```
// Assuming ID for player 1 is ABC-123-456

// Scoring a spare
{
  "game": {
    "ABC-123-456": {
      "turn1": 7,
      "turn2": 3
    }
  }
}

// Scoring a strike
{
  "game": {
    "ABC-123-456": {
      "turn1": 10,
      "turn2": 0
    }
  }
}

// Scoring an open frame
{
  "game": {
    "ABC-123-456": {
      "turn1": 1,
      "turn2": 6
    }
  }
}
```

##### Score logic
The API will reject all scores for a given turn, if the following scoring rules for each player is not followed:

```turn1``` must be a minimum of 0 and a maximum of 10 (inclusive).

```turn2``` must adhere to the following:

- be a value between 0 and 10 (inclusive)
- can not be more than the value of (10 - ```turn1```)
- may be ommitted entirely (or set as false), if it's value would otherwise be equal to zero

---

###### Some examples:

```turn1``` equals **3**, therefore - ```turn2``` may equal any of the following:

- any value from 0 - 7
- false (if otherwise equal to zero)
- undefined (if otherwise equal to zero)

---

```turn1``` equals **10**, therefore - ```turn2``` may equal any of the following:

- 0
- false (if otherwise equal to zero)
- undefined (if otherwise equal to zero)

---

```turn1``` equals **0**, therefore - ```turn2``` may equal any of the following:

- any value from 0 - 10
- false (if otherwise equal to zero)
- undefined (if otherwise equal to zero)

#### Response
Assuming that the Player IDs match up with the Game ID, *and* that the scores for this frame meet the criteria above - the API will respond with the following:

| | Description | Type |
|-|-------------|------|
| gameScore | Object containing the total score so far for the game, for each player, keyed on the player's Id | Object |
| framesHistory | Object, keyed on each player's ID, containing an object of frames and score information. | Object |

##### ```gameScore``` Object
The ```gameScore``` Object consists of the following:

| | Description | Type |
|-|-------------|------|
| ```:player1Id``` | The player 1's score for the game so far. | Integer |
| ```:player2Id``` | The player 2's score for the game so far. | Integer |

##### ```framesHistory``` Object
Each item in ```framesHistory``` is an object keyed on the frame number, that holds each player's score for that frame, keyed on the player's ID.

| | Description | Type |
|-|-------------|------|
| ```:player1Id``` | The player 1's score breakdown for the frame. | Object |
| ```:player2Id``` | The player 2's score breakdown for the frame. | Object |

Both the ```:player1Id``` and ```player2Id``` objects have the following structure:

| | Description | Type |
|-|-------------|------|
| turn1 | The number of pins knocked down in turn 1 of that frame. | Integer |
| turn2 | The number of pins knocked down in turn 2 of that frame. | Integer |
| turnsTotal | The total number of pins knocked down accross both turns in that frame | Integer |
| frameTotal | The score obtained for that frame (see below) | Integer |

The ```frameTotal``` is calculated as follows:
- If all 10 pins were knocked down on the first turn (a strike), then the frame total is equal to 10 + the sum of the pins knocked down on the next **two** turns after (within the next 2 frames).

- If all 10 pins were knocked down over two turns in that frame (a spare), then the frame total is equal to 10 + the sum of pins knocked down on the next **one** turn (in the next frame).

- If less than 10 pins were knocked down over two turns in that frame (an open frame), then the frame total is equal to the number of pins knocked down in that frame.

- If either a strike or a spare has been made in a frame, but the next frame has not yet completed, then ```frameTotal``` will be equal to the value of the pins knocked down in that frame only. This will still contribute to the total scores shown in ```gameScore```.


Example:
```
{
    "gameScore": {
        "Alex-1": 160,
        "Martin-2": 300
    },
    "framesHistory": {
        "Alex-1": {
            "1": {
                "turn1": 6,
                "turn2": 4,
                "turnsTotal": 10,
                "frameTotal": 13
            },
            "2": {
                "turn1": 3,
                "turn2": 0,
                "turnsTotal": 3,
                "frameTotal": 3
            },
            "3": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 27
            },
            "4": {
                "turn1": 8,
                "turn2": 1,
                "turnsTotal": 9,
                "frameTotal": 9
            },
            "5": {
                "turn1": 8,
                "turn2": 0,
                "turnsTotal": 8,
                "frameTotal": 8
            },
            "6": {
                "turn1": 7,
                "turn2": 2,
                "turnsTotal": 9,
                "frameTotal": 9
            },
            "7": {
                "turn1": 9,
                "turn2": 1,
                "turnsTotal": 10,
                "frameTotal": 20
            },
            "8": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "9": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 23
            },
            "10": {
                "turn1": 10,
                "turn2": 3,
                "turn3": 5,
                "turnsTotal": 18,
                "frameTotal": 18
            }
        },
        "Martin-2": {
            "1": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "2": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "3": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "4": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "5": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "6": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "7": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "8": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "9": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "10": {
                "turn1": 10,
                "turn2": 10,
                "turn3": 10,
                "turnsTotal": 30,
                "frameTotal": 30
            }
        }
    }
}
```

### ```GET``` /game/:gameId/?:frameNumber
Requests score information for each frame in the game. If ```:frameNumber``` hasn't been specified, then the API will return score information for all completed frames.

#### Request
URL parameters accept the following:

| Parameter | Description | Required |
|-----------|-------------|----------|
| ```:gameId``` | A valid game ID, as created from ```/new``` | true |
| ```:frameNumber``` | A frame of the game. Accepts any integer from 1 - 10. | false |

#### Response
Assuming a valid game ID, the API will respond with the following:

| | Description | Type |
|-|-------------|------|
| gameScore | Object containing the total score so far for the frames requested, for each player, keyed on the player's Id | Object |
| framesHistory | Object, keyed on each player's ID, containing an object of frames and score information. | Object |

##### ```gameScore``` Object
The ```gameScore``` Object consists of the following:

| | Description | Type |
|-|-------------|------|
| ```:player1Id``` | The player 1's score for the game so far. | Integer |
| ```:player2Id``` | The player 2's score for the game so far. | Integer |

##### ```framesHistory``` Object
Each item in ```framesHistory``` is an object keyed on the frame number, that holds each player's score for that frame, keyed on the player's ID.

| | Description | Type |
|-|-------------|------|
| ```:player1Id``` | The player 1's score breakdown for the frame. | Object |
| ```:player2Id``` | The player 2's score breakdown for the frame. | Object |

Both the ```:player1Id``` and ```player2Id``` objects have the following structure:

| | Description | Type |
|-|-------------|------|
| turn1 | The number of pins knocked down in turn 1 of that frame. | Integer |
| turn2 | The number of pins knocked down in turn 2 of that frame. | Integer |
| turnsTotal | The total number of pins knocked down accross both turns in that frame | Integer |
| frameTotal | The score obtained for that frame (see below) | Integer |

The ```frameTotal``` is calculated as follows:
- If all 10 pins were knocked down on the first turn (a strike), then the frame total is equal to 10 + the sum of the pins knocked down on the next **two** turns after (within the next 2 frames).

- If all 10 pins were knocked down over two turns in that frame (a spare), then the frame total is equal to 10 + the sum of pins knocked down on the next **one** turn (in the next frame).

- If less than 10 pins were knocked down over two turns in that frame (an open frame), then the frame total is equal to the number of pins knocked down in that frame.

- If either a strike or a spare has been made in a frame, but the next frame has not yet completed, then ```frameTotal``` will be equal to the value of the pins knocked down in that frame only. This will still contribute to the total scores shown in ```gameScore```.


Example:
```
{
    "gameScore": {
        "Alex-1": 160,
        "Martin-2": 300
    },
    "framesHistory": {
        "Alex-1": {
            "1": {
                "turn1": 6,
                "turn2": 4,
                "turnsTotal": 10,
                "frameTotal": 13
            },
            "2": {
                "turn1": 3,
                "turn2": 0,
                "turnsTotal": 3,
                "frameTotal": 3
            },
            "3": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 27
            },
            "4": {
                "turn1": 8,
                "turn2": 1,
                "turnsTotal": 9,
                "frameTotal": 9
            },
            "5": {
                "turn1": 8,
                "turn2": 0,
                "turnsTotal": 8,
                "frameTotal": 8
            },
            "6": {
                "turn1": 7,
                "turn2": 2,
                "turnsTotal": 9,
                "frameTotal": 9
            },
            "7": {
                "turn1": 9,
                "turn2": 1,
                "turnsTotal": 10,
                "frameTotal": 20
            },
            "8": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "9": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 23
            },
            "10": {
                "turn1": 10,
                "turn2": 3,
                "turn3": 5,
                "turnsTotal": 18,
                "frameTotal": 18
            }
        },
        "Martin-2": {
            "1": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "2": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "3": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "4": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "5": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "6": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "7": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "8": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "9": {
                "turn1": 10,
                "turn2": 0,
                "turnsTotal": 10,
                "frameTotal": 30
            },
            "10": {
                "turn1": 10,
                "turn2": 10,
                "turn3": 10,
                "turnsTotal": 30,
                "frameTotal": 30
            }
        }
    }
}
```
