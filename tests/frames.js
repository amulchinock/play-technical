// MOCHA DOESN'T SUPPORT ES6 OUT OF THE BOX. THIS TEST DOES NOT WORK.

import { expect } from 'chai'
import frameHelper from '../helpers/frames'

describe('frameHelper.frameNumber()', () => {
  it('should validate all valid frame numbers', () => {
    const validFrames = [
      null,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10
    ]

    for (let i = 0; i < validFrames.length; i++) {
      let supposedValidFrame = false
      frameHelper.frameNumber(validFrames[i]).then((frame) => {
        supposedValidFrame = frame
      })

      expect(supposedValidFrame).to.be.equal(validFrames[i])
    }
  })
})
