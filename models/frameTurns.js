import db from '../db'

class frameTurnsModel {
  fetchFrameTurns (frameId) {
    let query = 'SELECT * FROM frameturns WHERE frameturns.frameId = $1'

    return db.query(query, [frameId])
    .then(function (result) {
      return result.rows
    })
    .catch(function (error) {
      console.log(error)
    })
  }

  createFrameTurn (playerScores, frameId, turn) {
    let query = 'INSERT INTO frameturns (frameturns_id, frameid, turnnumber, player1score, player2score) VALUES(DEFAULT, $1, $2, $3, '
    let queryParams = [
      frameId,
      turn,
      playerScores[0]
    ]

    if (playerScores.length > 1) {
      query += '$4)'
      queryParams.push(playerScores[1])
    } else {
      query += 'DEFAULT)'
    }

    query += 'RETURNING *'

    return db.query(query, queryParams)
    .then(function (result) {
      return result.rows
    })
    .catch(function (error) {
      console.log(error)
    })
  }

  updateFrameTurn (frameTurnId, playerScores) {
    let query = 'UPDATE frameturns SET player1score = $1'
    let queryParams = [
      playerScores[0]
    ]

    if (playerScores.length > 1) {
      query += ', player2score = $2'
      queryParams.push(playerScores[1])
    }

    queryParams.push(frameTurnId)
    query += ' WHERE frameturns_id = '

    if (playerScores.length > 1) {
      query += '$3'
    } else {
      query += '$2'
    }

    query += ' RETURNING *'

    return db.query(query, queryParams)
    .then(function (result) {
      return result.rows
    })
    .catch(function (error) {
      console.log(error)
    })
  }
}

export default new frameTurnsModel()
