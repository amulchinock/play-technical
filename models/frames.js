import db from '../db'

class framesModel {
  checkFrameExists (playerToGameId, frameNumber) {
    let query = 'SELECT * FROM frames WHERE frames.playerToGameId = $1 AND frames.frameNumber = $2'
    let queryParams = [playerToGameId, frameNumber]

    return db.query(query, queryParams)
    .then(function (result) {
      return result.rows
    })
    .catch(function (error) {
      console.log(error)
    })
  }

  createNewFrame (playerToGameId, frameNumber) {
    let query = 'INSERT INTO frames(frames_id, playertogameid, framenumber) VALUES (DEFAULT, $1, $2) RETURNING *'
    let queryParams = [
      playerToGameId,
      frameNumber
    ]

    return db.query(query, queryParams)
    .then(function (result) {
      return result.rows
    })
    .catch(function (error) {
      console.log(error)
    })
  }
}

export default new framesModel()
