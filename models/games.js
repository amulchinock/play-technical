import db from '../db'

class gamesModel {
  createNewGame () {
    let query = 'INSERT INTO games(games_id) VALUES (DEFAULT) RETURNING *'

    return db.query(query)
    .then(function (result) {
      return result.rows[0]
    })
    .catch(function (error) {
      return error
    })
  }

  // Should this be in players model instead?
  assignPlayersToGame (players, game) {
    let query = 'INSERT INTO players_to_games(playersToGames_Id, gamesId, player1Id, player2Id) VALUES(DEFAULT, $1, $2, $3) RETURNING *'
    let queryParams = [
      game,
      players[0],
      players[1] ? players[1] : null // this MIGHT be a problem if a falsey value (0, for example) gets set as a player name?
    ]

    return db.query(query, queryParams)
    .then(function (result) {
      return result.rows
    })
    .catch(function (error) {
      return error
    })
  }

  fetchGameInfo (gameId, frame) {
    let query = 'SELECT * FROM games JOIN players_to_games ON (games.games_Id = players_to_games.gamesId) JOIN players ON (players_to_games.player1Id = players.players_Id OR players_to_games.player2Id = players.players_Id) JOIN frames ON (players_to_games.playersToGames_Id = frames.playerToGameId) JOIN frameTurns ON (frames.frames_Id = frameTurns.frameId) WHERE games.games_Id = $1 '
    let queryParams = [gameId]

    if (frame !== null) {
      query += 'AND frames.frameNumber <= $2 '
      queryParams.push(frame)
    }

    query += 'ORDER BY (frames.frameNumber, frameTurns.turnNumber)'

    return db.query(query, queryParams)
    .then(function (result) {
      return result.rows
    })
    .catch(function (error) {
      console.log(error)
    })
  }

  fetchGameConfig (gameId) {
    let query = 'SELECT * FROM games JOIN players_to_games ON (games.games_Id = players_to_games.gamesId) JOIN players ON (players_to_games.player1Id = players.players_id OR players_to_games.player2Id = players.players_id) WHERE games.games_Id = $1 '
    let queryParams = [gameId]

    return db.query(query, queryParams)
    .then(function (result) {
      return result.rows
    })
    .catch(function (error) {
      console.log(error)
    })
  }
}

export default new gamesModel()
