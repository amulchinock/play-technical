import db from '../db'

class playersModel {
  createNewPlayers (players) {
    let query = 'INSERT INTO players(players_id, playername) VALUES (DEFAULT, $1)'
    let queryParams = [
      players.player1
    ]

    if (players.hasOwnProperty('player2')) {
      query += ', (DEFAULT, $2) '
      queryParams.push(players.player2)
    }

    query += 'RETURNING *'

    return db.query(query, queryParams)
    .then(function (result) {
      return result.rows
    })
    .catch(function (error) {
      return error
    })
  }
}

export default new playersModel()
