class scoreChecker {
  calculateFrameScores (frames, playerNumber, historyMode = false) {
    // open, strike, spare
    let scoreTypeMinus1 = 'open' // score type achieved on previous turn
    let scoreTypeMinus2 = 'open' // score type achieved two turns ago

    let frameScores = {}

    for (let frame in frames) {
      if (frames.hasOwnProperty(frame)) {
        if ((parseInt(frame, 10) < 10 && frames[frame].hasOwnProperty(1) && frames[frame].hasOwnProperty(2)) || (parseInt(frame, 10) === 10 && frames[frame].hasOwnProperty(1) && frames[frame].hasOwnProperty(2) && frames[frame].hasOwnProperty(3))) {
          frameScores[frame] = (() => {
            let score = 0

            // Scoring addition for frames 1 - 9
            if (parseInt(frame, 10) < 10) {
              // Ignore second turn if we scored a strike
              // (being pedantic, as API shouldn't really let you send a second turn with a score greater than 0 anyway)
              if (frames[frame][1]['player' + playerNumber + 'score'] === 10) {
                score = frames[frame][1]['player' + playerNumber + 'score']
              } else {
                score = frames[frame][1]['player' + playerNumber + 'score'] + frames[frame][2]['player' + playerNumber + 'score']
              }
            }

            // Scoring addition for the 10th frame
            if (parseInt(frame, 10) === 10) {
              if (frames[frame][1]['player' + playerNumber + 'score'] === 10) {
                // Scored a strike, add remaining two turns
                score = frames[frame][1]['player' + playerNumber + 'score'] + frames[frame][2]['player' + playerNumber + 'score'] + frames[frame][3]['player' + playerNumber + 'score']

              } else if (frames[frame][1]['player' + playerNumber + 'score'] + frames[frame][2]['player' + playerNumber + 'score'] === 10) {
                // Scored a spare, add remaining turn
                score = frames[frame][1]['player' + playerNumber + 'score'] + frames[frame][2]['player' + playerNumber + 'score'] + frames[frame][3]['player' + playerNumber + 'score']

              } else {
                // Scored an open frame
                score = frames[frame][1]['player' + playerNumber + 'score'] + frames[frame][2]['player' + playerNumber + 'score']
              }
            }

            let returnVal = score

            if (historyMode) {
              // Seperated returnVal assignment, as per below, to ensure keys are
              // in the correct order when sent to the client. Purely aesthetic.

              returnVal = {
                turn1: frames[frame][1]['player' + playerNumber + 'score'],
                turn2: frames[frame][2]['player' + playerNumber + 'score']
              }

              if (parseInt(frame, 10) === 10) {
                returnVal.turn3 = frames[frame][3]['player' + playerNumber + 'score']
              }

              returnVal.turnsTotal = score
              returnVal.frameTotal = score
            }

            return returnVal
          })()


          // Updating previous frame scores (for strikes and spares), based on current frame score
          if (scoreTypeMinus1 === 'strike') {
            let updatedFrameScore = (() => {
              let score = 0

              if (historyMode) {
                score = frameScores[frame - 1].frameTotal
              } else {
                score = frameScores[frame - 1]
              }

              if (parseInt(frame, 10) < 10) {
                // If we throw a spare or an open frame in this frame
                if (frames[frame][1]['player' + playerNumber + 'score'] < 10) {
                  score = score + frames[frame][1]['player' + playerNumber + 'score'] + frames[frame][2]['player' + playerNumber + 'score']
                }

                // If we throw a strike in this frame
                if (frames[frame][1]['player' + playerNumber + 'score'] === 10) {
                  score = score + frames[frame][1]['player' + playerNumber + 'score']
                }
              }

              if (parseInt(frame, 10) === 10) {
                score = score + frames[frame][1]['player' + playerNumber + 'score'] + frames[frame][2]['player' + playerNumber + 'score']
              }

              return score
            })()


            if (historyMode) {
              frameScores[frame - 1].frameTotal = updatedFrameScore
            } else {
              frameScores[frame - 1] = updatedFrameScore
            }
          }

          if (scoreTypeMinus2 === 'strike') {
            let updatedFrameScore = (() => {
              let score = 0

              if (historyMode) {
                score = frameScores[frame - 2].frameTotal
              } else {
                score = frameScores[frame - 2]
              }

              // If we threw a strike in the previous frame, use the score from the first turn in this frame
              if (frames[frame - 1][1]['player' + playerNumber + 'score'] === 10) {
                score = score + frames[frame][1]['player' + playerNumber + 'score']
              }

              return score

            })()

            if (historyMode) {
              updatedFrameScore = (frameScores[frame - 2].frameTotal + frames[frame][1]['player' + playerNumber + 'score'])
              frameScores[frame - 2].frameTotal = updatedFrameScore
            } else {
              updatedFrameScore = (frameScores[frame - 2] + frames[frame][1]['player' + playerNumber + 'score'])
              frameScores[frame - 2] = updatedFrameScore
            }
          }

          if (scoreTypeMinus1 === 'spare') {
            let updatedFrameScore = 0

            if (historyMode) {
              updatedFrameScore = (frameScores[frame - 1].frameTotal + frames[frame][1]['player' + playerNumber + 'score'])
              frameScores[frame - 1].frameTotal = updatedFrameScore
            } else {
              updatedFrameScore = (frameScores[frame - 1] + frames[frame][1]['player' + playerNumber + 'score'])
              frameScores[frame - 1] = updatedFrameScore
            }
          }

          // Setting up flags for next pass through
          scoreTypeMinus2 = scoreTypeMinus1

          if (frames[frame][1]['player' + playerNumber + 'score'] === 10) {
            scoreTypeMinus1 = 'strike'
          } else if (frames[frame][1]['player' + playerNumber + 'score'] + frames[frame][2]['player' + playerNumber + 'score'] === 10) {
            scoreTypeMinus1 = 'spare'
          } else {
            scoreTypeMinus1 = 'open'
          }

          // if (historyMode) {
          //   frameScores[frame].scoreType = scoreTypeMinus1
          // }
        }
      }
    }

    return frameScores
  }


  orderFrames (playerTurns) {
    let frames = {}

    for (let frameNumber = 1; frameNumber <= 10; frameNumber++) {
      if (!frames.hasOwnProperty(frameNumber)) { // being pedantic here...
        frames[frameNumber] = (() => {
          let frameReturn = {}

          for (let turnNumber = 1; turnNumber <= 3; turnNumber++) {
            let turn = playerTurns.filter((el) => {
              return (el.framenumber === frameNumber && el.turnnumber === turnNumber)
            })

            if (turn.length > 0) { // only add turn if it exists
              if (!frameReturn.hasOwnProperty(turnNumber)) { // also being pedantic here
                frameReturn[turnNumber] = turn[0]
              }
            }
          }

          return frameReturn
        })()
      }
    }

    return frames
  }


  gameHistory (gameInfo) {
    // Get player IDs
    const player1Id = gameInfo[0].player1id
    const player2Id = gameInfo[0].player2id

    // console.log(player1Id, player2Id)

    const player1Turns = gameInfo.filter((el) => {
      return (el.players_id === player1Id)
    })

    const player2Turns = gameInfo.filter((el) => {
      return (el.players_id === player2Id)
    })

    // console.log(player2Turns)

    const playerNames = (() => {
      let returnObj = {}

      if (player1Turns.length > 0) {
        returnObj.player1 = player1Turns[0].playername
      }

      if (player2Turns.length > 0) {
        returnObj.player2 = player2Turns[0].playername
      }

      return returnObj
    })()

    console.log(playerNames)

    const frames = (() => {
      let returnObj = {}

      if (player1Turns.length > 0) {
        returnObj.player1 = this.orderFrames(player1Turns)
      }

      if (player2Turns.length > 0) {
        returnObj.player2 = this.orderFrames(player2Turns)
      }

      return returnObj
    })()

    let playerScores = (() => {
      let returnObj = {}

      if (frames.hasOwnProperty('player1')) {
        returnObj[playerNames.player1 + '-' + player1Id] = this.calculateFrameScores(frames.player1, 1, true)
      }

      if (frames.hasOwnProperty('player2')) {
        returnObj[playerNames.player2 + '-' + player2Id] = this.calculateFrameScores(frames.player2, 2, true)
      }

      return returnObj
    })()

    return playerScores
  }


  playerScore (playerTurns, playerNumber) {
    // Order turns by frame, then turn number
    const frames = this.orderFrames(playerTurns)

    // Iterate through frames and turns to calculate scores
    const frameScores = this.calculateFrameScores(frames, playerNumber)

    return (() => {
      let playerGameTotal = 0

      for (let frame in frameScores) {
        if (frameScores.hasOwnProperty(frame)) {
          playerGameTotal = playerGameTotal + frameScores[frame]
        }
      }

      return playerGameTotal
    })()
  }
}

const scoreHelper = new scoreChecker()
export default scoreHelper
