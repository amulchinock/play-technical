class playerChecker {
  validatePlayers (players) {
    return new Promise((resolve, reject) => {
      if (players.hasOwnProperty('players')) {
        if (players.players.hasOwnProperty('player1')) {
          let playerErrorMessage  = null
          if (players.players.player1 === '' || !players.players.player1) {
            playerErrorMessage = 'Player 1\'s name '
          }

          if (players.players.hasOwnProperty('player2')) {
            if (players.players.player2 === '' || !players.players.player2) {
              playerErrorMessage = 'and Player 2\'s name '
            }
          }

          if (playerErrorMessage !== null) {
            playerErrorMessage += 'cannot be a falsey value or left blank.'

            reject({
              success: false,
              message: playerErrorMessage
            })
          } else {
            resolve(players.players)
          }

        } else {
          reject({
            success: false,
            message: 'You need to specify player1'
          })
        }
      } else {
        reject({
          success: false,
          message: 'Please specify a players object in your request'
        })
      }
    })
  }
}

const playerHelper = new playerChecker()
export default playerHelper
