class frameChecker {
  frameNumber (frame) {
    return new Promise((resolve, reject) => {
      if ((frame > 10 || frame < 1) && frame !== null) {
        reject('Frame number must be between 1 and 10')
      } else {
        resolve(frame)
      }
    })
  }
}

const frameHelper = new frameChecker()
export default frameHelper
