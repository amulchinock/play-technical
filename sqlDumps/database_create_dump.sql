CREATE TABLE games (
	games_Id SERIAL PRIMARY KEY
);

CREATE TABLE players (
	players_Id SERIAL PRIMARY KEY,
	playerName varchar(80) NOT NULL
);

CREATE TABLE players_to_games (
	playersToGames_Id SERIAL PRIMARY KEY,
	gamesId INTEGER REFERENCES games(games_Id),
	player1Id INTEGER REFERENCES players(players_Id),
	player2Id INTEGER REFERENCES players(players_Id) CHECK (player1Id != player2Id)
);

CREATE TABLE frames (
	frames_Id SERIAL PRIMARY KEY,
	playerToGameId INTEGER REFERENCES players_to_games(playersToGames_Id),
	frameNumber INTEGER CHECK (frameNumber BETWEEN 1 AND 10)
);

CREATE TABLE frameTurns (
	frameTurns_Id SERIAL PRIMARY KEY,
	frameId	INTEGER REFERENCES frames(frames_Id),
	turnNumber INTEGER CHECK (turnNumber BETWEEN 1 AND 3),
	player1Score INTEGER CHECK (player1Score BETWEEN 0 AND 10),
	player2Score INTEGER CHECK (player2Score BETWEEN 0 AND 10)
);
