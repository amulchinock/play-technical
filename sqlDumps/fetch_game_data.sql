SELECT * FROM games
JOIN players_to_games ON (games.games_Id = players_to_games.gamesId)
JOIN players ON (players_to_games.player1Id = players.players_Id OR players_to_games.player2Id = players.players_Id)
JOIN frames ON (players_to_games.playersToGames_Id = frames.playerToGameId)
JOIN frameTurns ON (frames.frames_Id = frameTurns.frameId)
WHERE games.games_Id = 1
ORDER BY (frames.frameNumber, frameTurns.turnNumber);
